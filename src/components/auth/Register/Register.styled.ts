"use client";

import styled from "@emotion/styled";
import Image from "next/image";


export const ErrorIcon = styled(Image)`
  position: absolute;
  right: 10px;
  top: 50%;
  transform: translateY(-50%);
`;
