import { AboutMe } from "@/components";
import "../../globals.scss";

export default function Page() {
  return (
    <div className="mobileHidden">
      <AboutMe />
    </div>
  );
}
